const webpack = require('webpack');

export default {
  /*
  ** Nuxt rendering mode
  ** See https://nuxtjs.org/api/configuration-mode
  */
  mode: 'spa',
  /*
  ** Nuxt target
  ** See https://nuxtjs.org/api/configuration-target
  */
  target: 'static',
  /*
  ** Headers of the page
  ** See https://nuxtjs.org/api/configuration-head
  */
  head: {
    title: process.env.npm_package_name || '',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: process.env.npm_package_description || '' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ],
    // script: [
    //   { src: 'https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js', body: true },
    // ]
  },
  /*
  ** Global CSS
  */
  css: [
    'slick-carousel/slick/slick.css',
    'slick-carousel/slick/slick-theme.css',
    '~assets/SCSS/main.scss'
  ],
  /*
  ** Plugins to load before mounting the App
  ** https://nuxtjs.org/guide/plugins
  */
  plugins: [

    //'~/plugins/jquery',
    '~/plugins/slick-carousel',
    //'~/plugins/vue-jquery',

    // {src: '~/plugins/slick-carousel', mode: 'client'},


  ],
  /*
  ** Auto import components
  ** See https://nuxtjs.org/api/configuration-components
  */
  components: true,
  /*
  ** Nuxt.js dev-modules
  */
  buildModules: [
    // '@nuxtjs/fontawesome',

  ],
  // fontawesome: {
  //   icons: {
  //     solid: ['faCog'],
  //     ...
  //   }
  // },
  /*
  ** Nuxt.js modules
  */
  modules: [
    '@nuxtjs/style-resources',

    // Doc: https://bootstrap-vue.js.org
    'bootstrap-vue/nuxt',
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    '@nuxtjs/pwa',


    ['nuxt-fontawesome', {
      component: 'fa',
      imports: [
        {
        // <i class="fab fa-wordpress"></i>
          set: '@fortawesome/free-solid-svg-icons',
          icons: ['faDollarSign','faAnchor','faCircle','faPhoneAlt','faHome','faComments', 'faBlog','faUser',
            'faLifeRing','faTasks','faRandom','faHeart','faShoppingCart','faMobile','faEnvelope']
        }
      ],
      regular: true
    }]
  ],

  // fontawesome: {
  //   imports: [
  //     {
  //       set: '@fortawesome/free-solid-svg-icons', // Solid icons
  //       icons: ['faCookieBite', 'faCommentDots', 'faEnvelope', 'faGrinWink', 'faHeart']
  //     },
  //     {
  //       set: '@fortawesome/free-brands-svg-icons', // Brand icons
  //       icons: ['faDev', 'faFacebook', 'faTwitter', 'faInstagram', 'faYoutube', 'faGithub']
  //     }
  //   ]
  // },

  styleResources: {
    scss: './assets/SCSS/variables.scss'
  },
  /*
  ** Axios module configuration
  ** See https://axios.nuxtjs.org/options
  */
  axios: {},
  /*
  ** Build configuration
  ** See https://nuxtjs.org/api/configuration-build/
  */
  build: {
    // vendor: ["jquery"],
    // plugins: [
    //   new webpack.ProvidePlugin({
    //     $: "jquery",
    //     "jQuery": "jquery"
    //   })
    // ],


    vendor: ["jquery", "bootstrap", "vue-i18n"],
    plugins: [
      new webpack.ProvidePlugin({
        $: "jquery",
        "jQuery": "jquery"
      })
    ],
  }
}
